# Practica Final Javascript
## Descripcion

En esta practica se trabajo con los fundamentos basicos del lenguaje Javascript, incluyendo: 
- Manejo del DOM.
- AJAX.
- Uso de la libreria JQuery.

El objetivo de este ejercicio era completar la seccion 'Buscar', en la cual se tenian que mostrar las coincidencias de las peliculas con la entrada del usuario de manera interactiva y sin tener que recargar la pagina. 

## Solucion
Para solucionar esta problema se trabajo con dos eventos: 

- onclick
- keyup

El evento onclick lo utilizamos al pulsar el boton "Agregar" para mostrar en la pantalla una seria de tarjetas, que incluyen imagen y titulo, de las peliculas coincidentes a la palabra ingresada en el input. 

Para colocar de mejor manera las tarjetas de la lista, se le asigno un display flex al contenedor ul. 

El evento keyup lo utilizamos para detectar el ingreso de una nueva letra en el input para la busqueda, esto permite mostrar los titulos de las peliculas para poder ayudar a completar la busqueda y hacer la busqueda mas precisa al darle click al boton "Agregar".

Dentro de cada evento se realiza una llamada a la API para obtener cada que sucede el evento la informacion de las peliculas. 

### Autor

Vazquez Sanchez Erick Alejandro.