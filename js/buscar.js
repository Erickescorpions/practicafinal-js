

$(document).ready(function(){

	let miLista = $("#miLista");
	let busqueda = $("#busqueda");	
	let apiURL = "https://api.themoviedb.org/3/movie/upcoming?api_key=3356865d41894a2fa9bfa84b2b5f59bb&language=es";
	let listaCoincidencias = $("#lista-coincidencias");
	$("#lista-coincidencias").hide(0);


	/**
	 * 
	 * Este evento muestra en forma de tarjetas todas las coincidencias que existan con 
	 * la palabra que se ingreso en el input de la busqueda
	 * 
	 */
	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();

		$.ajax({
			url: apiURL,
			success: function(res) {
				setTimeout(function () {
					
					$('#loader').remove();
					miLista.empty();

					$.each(res.results, function(index, elemento) {
						if(elemento.original_title.includes(palabra)) {
							miLista.append(crearMovieCard(elemento));
						}
					});
					
				}, 2000);

			},
			
			error: function() {
				$('#loader').remove();
				miLista.html('No se ha podido obtener la información');
			},
	
			beforeSend: function() { 
				listaCoincidencias.hide(500);
				//ANTES de hacer la petición se muestra la imagen de cargando.
				miLista.html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
			},
		});
	});

	

	/** 
	 * 
	 * Este evento te muestra en forma de lista todas las coincidencias que vayan apareciendo
	 * de manera interactiva para que el usuario pueda completar su busqueda. 
	 * 
	*/
	busqueda.keyup( function() {
		const palabra = busqueda.val();

		$.ajax({
			url: apiURL,
			success: function(res) { 
				listaCoincidencias.empty();
				listaCoincidencias.show(1000);

				$.each(res.results, function(i, elem) {
					if(elem.original_title.includes(palabra)) {
						listaCoincidencias.append( () => `<div> ${elem.original_title} </div>` );
					}	
				});
			},
			
			error: function() {
				miLista.html('No se ha podido obtener la información');
			},

		});

	});

});


function crearMovieCard(movie) {
	return '<!-- CARD -->'
		+ '<li class="col-md-4">'
			+ '<div>'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		       +'</div>'
		    +'</div>'
			+ '</div>'
		+ '</li>'
		+'<!-- CARD -->';
}